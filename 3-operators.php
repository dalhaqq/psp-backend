<?php
// error_reporting(E_ERROR | E_PARSE);

// // aritmatika
// $var = "123";
// $var2 = "45r";

// $var3 = $var + $var2;
// echo "$var + $var2 = $var3<br>";
// echo $var3;
// echo "<br>";

// $var3 = $var - $var2;
// echo "$var - $var2 = $var3<br>";
// echo $var3;
// echo "<br>";

// $var3 = $var * $var2;
// echo "$var * $var2 = $var3<br>";
// echo $var3;
// echo "<br>";

// $var3 = $var / $var2;
// echo "$var / $var2 = $var3<br>";
// echo $var3;
// echo "<br>";

function check($bool)
{
    if ($bool) {
        echo "TRUE<br>";
    } else {
        echo "FALSE<br>";
    }
}

// operator logika
// $var = TRUE;
// $var2 = FALSE;
// check($var && $var2);

// operator perbandingan
// $var = 123;
// $var2 = "123";
// check($var !== $var2);

$var = 123;
$var2 = 321;
check($var < $var2);

// operator assignment
// $var = 123;
// $var += 321; // $var = $var + 321;
// echo $var . "<br>";

// $string = "Hello ";
// $string .= "world!";
// echo $string . "<br>";

// // operator increment
// $var = 123;
// echo ++$var . "<br>";
// echo $var++ . "<br>";
// echo $var . "<br>";

// // operator decrement
// $var = 123;
// echo --$var . "<br>";
// echo $var-- . "<br>";
// echo $var . "<br>";

// // operator string
// $var = "Hello ";
// $var2 = "world!";
// echo $var . $var2 . "<br>";