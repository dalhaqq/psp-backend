<?php

declare(strict_types=1);

// parameter fungsi dapat memiliki type tertentu
// fungsi dapat memiliki return type tertentu
function sayHello(string $name): string
{
    return "Hello, $name!<br>";
}

sayHello("saih");

$array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function multiplyByTwo(int $x)
{
    echo $x * 2 . '<br>';
}

// fungsi dapat menjadi parameter dari fungsi lain
// menggunakan nama fungsi
array_map('multiplyByTwo', $array);

// mendefinisikan parameter berupa fungsi, menggunakan type callable
function generateHello(callable $generator, string $name)
{
    return $generator($name);
}

echo generateHello('sayHello', 'Saih') . '<br>';

function sayHalo($nama)
{
    return "Halo, $nama!";
}

echo generateHello('sayHalo', 'Saih') . '<br>';

// fungsi dapat dibuat tanpa nama (anonymous function)
// fungsi ini disimpan ke sebuah variabel
$kaliDua = function ($x) {
    echo $x * 2 . '<br>';
};


// menggunakan variabel fungsi
array_map($kaliDua, $array);

// menggunakan fungsi anonymous 
array_map(function ($x) {
    echo $x * $x . "<br>";
}, $array);


// anonymous function tidak dapat mengakses variabel global kecuali ditentukan menggunakan keyword use
$printArray = function () use ($array) {
    foreach ($array as $x) {
        echo "$x <br>";
    }
};

// melakukan pemanggilan pada anonymous function yang disimpan ke variabel
$printArray();