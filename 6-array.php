<?php

// indexed array
$hari = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'];
$hari2 = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');

// mendefinisikan array kosong
$hari3 = [];
$hari4 = array();

// menambahkan isi array
foreach ($hari as $h) {
    $hari3[] = $h;
}

foreach ($hari2 as $h) {
    array_push($hari4, $h);
}
print_r($hari4);

echo '<br>';

// mengganti nilai pada index tertentu
$hari3[0] = 'Monday';
print_r($hari3);

echo '<br>';

// menghapus nilai dari array
unset($hari3[0]);
print_r($hari3);

echo '<br>';

// pop, pasangan push
// push  -> menambahkan nilai ke akhir array
// pop   -> menghapus nilai dari akhir array dan mengembalikan nilai yang dihapus
$h = array_pop($hari4);
print_r($hari4);
echo '<br>';
echo $h;

echo '<br>';

// associative array
$translate = [
    'Senin' => 'Monday',
    'Selasa' => 'Tuesday',
    'Rabu' => 'Wednesday',
    'Kamis' => 'Thursday',
    'Jumat' => 'Friday',
    'Sabtu' => 'Saturday',
    'Minggu' => 'Sunday'
];

$translate2 = array(
    'Senin' => 'Monday',
    'Selasa' => 'Tuesday',
    'Rabu' => 'Wednesday',
    'Kamis' => 'Thursday',
    'Jumat' => 'Friday',
    'Sabtu' => 'Saturday',
    'Minggu' => 'Sunday'
);

echo $translate['Senin'] . '<br>';

// menambahkan nilai ke associative array
$translate['Hari'] = 'Day';
print_r($translate);

echo '<br>';

// menghapus nilai dari associative array
unset($translate['Hari']);
print_r($translate);

// mengambil key dari associative array
echo '<br>';
print_r(array_keys($translate));

// mengambil value dari associative array
echo '<br>';
print_r(array_values($translate));

// mencari nilai pada suatu array
echo '<br>';
echo array_search('Monday', $translate);

echo '<br>';
echo array_search('Senin', $hari);

echo '<br>';
$hasil = array_search('Monday', $hari);
if ($hasil !== false) {
    echo 'Monday ada di index ' . $hasil;
} else {
    echo 'Monday tidak ada di array';
}

// memisahkan string dan menggabungkan array
echo '<br>';
$data = 'Sunday Monday Tuesday Wednesday Thursday Friday Saturday';
$days = explode(' ', $data);
print_r($days);

echo '<br>';
echo "There are seven days in a week, they are " . implode(', ', $days);
// join(', ', $days);

// mengambil nilai unik dari array
echo '<br>';
print_r(array_unique([1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2]));

// menggabungkan array
echo '<br>';
print_r(array_merge([1, 2, 3], [4, 5, 6], [7, 8, 9]));

// mengurutkan array
echo '<br>';
array_multisort($hari, SORT_DESC);
print_r($hari);

// filtering array
echo '<br>';
print_r(array_filter($hari, function ($value) {
    return $value[0] == 'S';
}));

echo '<br>';
$data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
print_r(array_filter($data, function ($value) {
    return $value % 2 == 0;
}));