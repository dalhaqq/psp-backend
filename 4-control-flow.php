<?php

if (true) {
    echo "TRUE<br>";
}

if (true) {
    echo "TRUE<br>";
} else {
    echo "FALSE<br>";
}

$var = 3 + 1;

if ($var == 8) {
    echo "3 + 1 = 8<br>";
} else if ($var < 8) {
    echo "3 + 1 < 8<br>";
} else {
    echo "3 + 1 > 8<br>";
}

$variable = "Selasa";
switch ($variable) {
    case "Senin":
        echo 1;
        break;
    case "Selasa":
        echo 2;
        break;
    case "Rabu":
        echo 3;
        break;
    case "Kamis":
        echo 4;
        break;
    case "Jumat":
        echo 5;
        break;
    case "Sabtu":
        echo 6;
        break;
    case "Minggu":
        echo 7;
        break;
    default:
        echo "Hari tidak ditemukan";
        break;
}

$a = 1;

do {
    echo $a . "<br>";
    $a++;
} while ($a >= 10);

for (;$a <= 10;$a += 2) { 
    echo $a . "<br>";
}

$arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
$assoc = [
    "nama" => "Rizky",
    "umur" => 20,
    "alamat" => "Jl. Jalan"
];

foreach ($arr as $index => $nilai) {
    echo "arr[$index] = $nilai<br>";
}