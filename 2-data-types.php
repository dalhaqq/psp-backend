<?php

// string
// $var = "Hello, world!";
// echo $var . "<br>";
// echo "$var<br>";
// echo '$var<br>';
// echo gettype($var) . "<br>";

// $var = 123.5;
// echo $var . "<br>";
// echo gettype($var) . "<br>";

// $var = TRUE;
// echo $var . "<br>";
// echo gettype($var) . "<br>";

// $var = 123;
// echo $var . "<br>";
// echo gettype($var) . "<br>";

// $var = ["Hello", "world"];
// print_r($var);
// echo gettype($var) . "<br>";

// $var = new stdClass();
// $var->name = "Hello";
// print_r($var);
// echo gettype($var) . "<br>";

$var = null;
echo gettype($var) . "<br>";