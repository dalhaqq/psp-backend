<?php
// // variable
// $var = "Hello, world!<br>";
// // echo $var;
// function hello()
// {
//     global $var;
//     $var = "Hello global!<br>";
//     echo $var;
// }

// hello();
// echo $var;

// $var2 = "Hello, world!<br>";
// function hello2()
// {
//     $var2 = "Hello local!<br>";
//     echo $var2;
// }

// hello2();
// echo $var2;

function hitung() {
    static $i = 0;
    $i++;
    echo $i . '<br>';
}
echo 'Static:<br>';
hitung();
hitung();

function hitung2() {
    $i = 0;
    $i++;
    echo $i . '<br>';
}
echo 'Non-static:<br>';
hitung2();
hitung2();

?>